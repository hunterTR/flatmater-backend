/**
 * Created by cem.kaya on 26-Oct-15.
 */
var express = require('express');
var path = require('path');
var app = express();
var roothPath = path.normalize(__dirname + '/../');

app.use(express.static(roothPath));
app.listen(9000);
console.log('listening on port 9000');