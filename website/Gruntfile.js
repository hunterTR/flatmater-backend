module.exports = function (grunt) {
    grunt.initConfig({


        concat: {
            js: { //target
                src: [
                    'js/bootstrap.min.js',
                    'js/classie.js',
                    'js/cbpAnimatedHeader.js',
                    'js/dropzone.js',
                    'js/agency.js',
                    'js/jqBootstrapValidation.js',
                    'js/contact_me.js',

                    'bower_components/angular/angular.js',
                    'bower_components/angular-animate/angular-animate.js',
                    'bower_components/angular-cookies/angular-cookies.js',
                    'bower_components/angular-resource/angular-resource.js',
                    'bower_components/angular-route/angular-route.js',
                    'bower_components/angular-sanitize/angular-sanitize.js',
                    'bower_components/angular-touch/angular-touch.js',
                    'bower_components/angular-messages/angular-messages.js',
                    'bower_components/angular-material/angular-material.js',
                    'bower_components/angular-aria/angular-aria.js',
                    'bower_components/ng-flow/dist/ng-flow-standalone.js',
                    'js/ui-bootstrap-tpls-0.14.3.js',
                    'bower_components/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.js',
                    'bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
                    'bower_components/textAngular/dist/textAngular-rangy.min.js',
                    'bower_components/textAngular/dist/textAngular-sanitize.min.js',
                    'bower_components/textAngular/dist/textAngular.min.js',
                    'bower_components/ngmap/build/scripts/ng-map.js',
                    'bower_components/ng-facebook/ngFacebook.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.js',
                    'bower_components/angular-ui-mask/dist/mask.js',
                    'bower_components/angular-translate/angular-translate.js',
                    'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
                    'bower_components/angular-scroll-glue/src/scrollglue.js',
                    'bower_components/angulartics/dist/angulartics.min.js',
                    'bower_components/angulartics-google-analytics/dist/angulartics-google-analytics.min.js',
                    'bower_components/angular-md5/angular-md5.js',
                    'bower_components/angulargrid/angulargrid.js',


                    'app/app.core.js',
                    'app/app.module.js',
                    'app/components/components.module.js',
                    'app/components/listmodalinstance.controller.js',
                    'app/components/carousel.controller.js',
                    'app/components/footer.controller.js',
                    'app/components/reportModal/reportModal.controller.js',
                    'app/components/reportModal/report.service.js',
                    'app/components/textShowModal/textShowModal.controller.js',
                    'app/components/htmltop.controller.js',
                    "app/pages/loading/loading.module.js",
                    "app/pages/loading/loading.controller.js",
                    "app/utilities/utils.module.js",
                    "app/utilities/cookies.service.js",
                    "app/utilities/authentication.service.js",
                    "app/utilities/application.service.js",
                    "app/utilities/information.service.js",
                    "app/utilities/passwordmatch.directive.js",
                    "app/utilities/convertToNumber.directive.js",
                    "app/utilities/dropzone.directive.js",
                    "app/utilities/fileupload.directive.js",
                    "app/utilities/date.filter.js",
                    "app/utilities/language.service.js",
                    "app/utilities/randomString.service.js",
                    "app/utilities/scroll.service.js",

                    "app/pages/login/login.module.js",
                    "app/pages/login/login.controller.js",
                    "app/pages/login/passwordForgot/passwordForgot.controller.js",

                    "app/pages/navbar/navbar.module.js",
                    "app/pages/navbar/navbar.controller.js",


                    "app/pages/main/main.module.js",
                    "app/pages/main/main.controller.js",

                    "app/pages/register/register.module.js",
                    "app/pages/register/register.controller.js",
                    "app/pages/register/hobbiesModal/hobbiesModal.directive.js",
                    "app/pages/register/sportsModal/sportsModal.directive.js",
                    "app/pages/register/personalPhotosModal/personalphotosModal.directive.js",

                    "app/pages/listing/listing.module.js",
                    "app/pages/listing/listing.controller.js",
                    "app/pages/listing/listElement/listElement.directive.js",
                    "app/pages/listing/listElement/listModal.directive.js",
                    "app/pages/listing/listing.service.js",
                    "app/pages/listing/searchModal/searchModal.directive.js",
                    "app/pages/listing/listElement/listElement.controller.js",

                    "app/pages/category/category.module.js",
                    "app/pages/category/category.controller.js",
                    "app/pages/category/category.service.js",

                    "app/pages/profile/profile.module.js",
                    "app/pages/profile/modal/profileModal.controller.js",
                    "app/pages/profile/modal/profileModal.directive.js",
                    "app/pages/profile/modal/personality/personality.controller.js",
                    "app/pages/profile/modal/personality/personality.directive.js",
                    "app/pages/profile/modal/house/house.directive.js",
                    "app/pages/profile/modal/house/house.controller.js",
                    "app/pages/profile/modal/about/about.directive.js",

                    "app/pages/profile/edit/edit.module.js",
                    "app/pages/profile/edit/edit.controller.js",
                    "app/pages/profile/edit/edit.service.js",
                    "app/pages/profile/edit/personalinfo/personalinfo.directive.js",
                    "app/pages/profile/edit/houseEdit/houseEdit.directive.js",
                    "app/pages/profile/edit/informationsEdit/informationsEdit.directive.js",
                    "app/pages/profile/edit/uploadModals/uploadModal.directive.js",
                    "app/pages/profile/edit/photoGallery/photoGallery.directive.js",
                    "app/pages/profile/edit/houseEdit/houseEdit.controller.js",

                    "app/pages/test/test.module.js",
                   "app/pages/test/test.controller.js",

                    "app/pages/profile/profile.controller.js",
                    "app/pages/profile/profile.service.js",

                    "app/pages/settings/settings.module.js",
                    "app/pages/settings/settings.controller.js",
                    "app/pages/settings/settings.service.js",

                    "app/pages/messages/messages.module.js",
                    "app/pages/messages/messages.controller.js",
                    "app/pages/messages/messages.service.js",
                    "app/pages/messages/messageModal/messageModal.controller.js",

                    "app/pages/verification/verification.module.js",
                    "app/pages/verification/verification.controller.js",
                    "app/pages/verification/verification.service.js",

                    "app/pages/password/password.module.js",
                    "app/pages/password/password.controller.js",
                    "app/pages/password/password.service.js",

                    "app/components/sendMessageModal/sendMessageModal.controller.js"


                ],
                dest: 'dest/concat.js'
            }
        },

        uglify: {
            my_target: {
                files: {
                    'dest/minified.js': ['dest/concat.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify'); // load the given tasks
    grunt.registerTask('default', ['concat','uglify']); // Default grunt tasks maps to grunt
};