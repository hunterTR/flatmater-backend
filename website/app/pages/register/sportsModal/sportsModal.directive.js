/**
 * Created by cem on 07/11/15.
 */

angular.module('app.register')
    .directive("sportsModal", function () {
        return{
            restrict: 'E',
            replace:true,
            templateUrl: 'app/pages/register/sportsModal/sportsModal.html'
        }

    });