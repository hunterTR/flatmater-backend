/**
 * Created by cem.kaya on 07-Nov-15.
 */
angular.module('app.register')
    .directive("personalphotosModal", function () {
        return{
            restrict: 'E',
            replace:true,
            templateUrl: 'app/pages/register/personalPhotosModal/personalphotosModal.html',

        }

    });