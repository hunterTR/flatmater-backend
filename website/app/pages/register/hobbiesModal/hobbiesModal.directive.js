/**
 * Created by cem on 07/11/15.
 */

angular.module('app.register')
    .directive("hobbiesModal", function () {
        return{
            restrict: 'E',
            replace:true,
            templateUrl: 'app/pages/register/hobbiesModal/hobbiesModal.html',

        }

    });