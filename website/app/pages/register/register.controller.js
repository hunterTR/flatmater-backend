/**
 * Created by cem.kaya on 27-Oct-15.
 */
'use strict'
angular.module("app.register").
controller("RegisterCtrl",['AuthenticationService', 'InformationService', '$window', 'md5','religiousViews','politicalViews','horoscopes','hobbies','sports','budgets','organizations', function (AuthenticationService, InformationService, $window, md5,religiousViews,politicalViews,horoscopes,hobbies,sports,budgets,organizations) {
    var vm = this;
    vm.user = {};
    vm.user.personalityphotos = [];
    vm.isLoading = false;
    vm.informations = {};
    vm.informations.hobbies = [];
    vm.informations.sports = [];
    vm.test = "test";
    vm.userPriorities = InformationService.getPriorities();
    vm.error = false;
    vm.toggleCheckbox = toggleCheckbox;
    vm.onCityChange = onCityChange;
    vm.onDistrictChange = onDistrictChange;
    vm.cityNameOnChange = cityNameOnChange;
    vm.saveHobbies = saveHobbies;
    vm.saveSports = saveSports;

    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);

    console.log(vm.userPriorities);
    vm.nextRegister = function () {
        vm.isLoading = true;
        vm.user.informations = vm.informations;
        vm.user.password = md5.createHash(vm.user.password);
        AuthenticationService.register(vm.user)
            .then(registerResponse)
            .catch(registerError);

        function registerResponse(res) {
            if (res.success === true) {
                console.log(res);
                AuthenticationService.login(vm.user.email, vm.user.password, function (data) {

                    console.log(vm.user.email + vm.user.password);
                    if (data.success === true) {
                        console.log(data);
                        $window.location.reload();
                    }
                    else {
                        console.log(data.message);
                        vm.error = true;
                        vm.errorMessage = data.message;
                        vm.isLoading = false;
                    }

                });
            }
            else {
                console.log(res);
                vm.error = true;
                vm.errorMessage = res.message;
                vm.isLoading = false;
            }
        }

        function registerError(err) {
            console.log(err);
            vm.isLoading = false;
        }

    };

    function toggleCheckbox(item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);

        console.log(vm.user.personalityphotos);
    }

    function onCityChange() {
        var place = this.getPlace();
        console.log('seçildi');
        vm.informations.city = place.name;
    }

    function onDistrictChange() {
        var place = this.getPlace();
        console.log('seçildi');
        vm.informations.district = place.name;

    }

    function saveHobbies() {
        vm.hobbiesSaved = true;
        vm.saveMessage = "register_modal_hobbies";
    }

    function saveSports() {
        vm.sportsSaved = true;
        vm.saveMessage = "register_modal_sports";

    }



    function cityNameOnChange() {
        register.city.$valid = false;
    }

    vm.religions = religiousViews;

    vm.politics = politicalViews;

    vm.organizations = organizations;

    vm.budgets = budgets;

    vm.horoscopes = horoscopes;

    vm.sports = sports;

    vm.hobbies = hobbies;


    vm.personalPhotos = [
        {
            tooltip: "categories_gamer_desc",
            img: "img/portfolio/gamer.jpg"
        },
        {
            tooltip: "categories_music_desc",
            img: "img/portfolio/musician.jpg"
        },
        {
            tooltip: "categories_pet_desc",
            img: "img/portfolio/petowner.jpg"
        },
        {
            tooltip: "categories_party_desc",
            img: "img/portfolio/partylover.jpg"
        },
        {
            tooltip: "categories_chef_desc",
            img: "img/portfolio/chef.jpg"
        },
        {
            tooltip: "categories_smoker_desc",
            img: "img/portfolio/smoker.jpg"
        },
        {
            tooltip: "categories_lgbt_desc",
            img: "img/portfolio/gayfriendly.jpg"
        },
        {
            tooltip: "categories_traveler_desc",
            img: "img/portfolio/traveler.jpg"
        },
        {
            tooltip: "categories_sport_desc",
            img: "img/portfolio/sport.jpg"
        }, {
            tooltip: "categories_night_desc",
            img: "img/portfolio/owl.jpg"
        },
        {
            tooltip: "categories_neat_desc",
            img: "img/portfolio/temiz.jpg"
        },
        {
            tooltip: "categories_relax_desc",
            img: "img/portfolio/messy.jpg"
        }

    ];
    //
    //vm.personalPhotos = ["img/portfolio/gamer.jpg", "img/portfolio/musician.jpg",
    //    "img/portfolio/petowner.jpg", "img/portfolio/partylover.jpg", "img/portfolio/chef.jpg",
    //    "img/portfolio/smoker.jpg", "img/portfolio/gayfriendly.jpg", "img/portfolio/traveler.jpg","img/portfolio/sport.jpg",
    //    "img/portfolio/owl.jpg", "img/portfolio/temiz.jpg", "img/portfolio/messy.jpg"];


}]).config(['$mdThemingProvider',function ($mdThemingProvider) {

    // Configure a dark theme with primary foreground yellow

    $mdThemingProvider.theme('docs-dark', 'default')
        .primaryPalette('yellow')
        .dark();


}]);