/**
 * Created by cem.kaya on 28-Nov-15.
 */
'use strict';

angular.module('app.messages')
    .service('MessagesService',['$http','CookiesService', function ($http,CookiesService) {

        var service ={
            getMessages:getMessages,
            sendMessage:sendMessage,
            checkNotifications:checkNotifications,
            getMessageById:getMessageById
        };


        return service;

        function getMessages(){
            var processUrl = "/api/getmessages";

            return $http.post(processUrl,{},{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }

        }

        function checkNotifications(){
            var processUrl = "/api/checknotifications";

            return $http.post(processUrl,{},{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }
        }

        function getMessageById(id){
            var processUrl = "/api/getmessagebyid";

            var data = {messageid:id};
            return $http.post(processUrl,data,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }
        }

        function sendMessage(data){
            var processUrl = "/api/sendmessage";

            return $http.post(processUrl,data,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }
        }

    }]);
