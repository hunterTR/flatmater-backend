/**
 * Created by cem.kaya on 28-Nov-15.
 */
'use strict';
angular.module("app.messages").
controller("MessageModalCtrl",['$uibModalInstance','MessagesService','message', function ($uibModalInstance,MessagesService,message) {
    var vm = this;

    vm.message = message;
    vm.sendMessage = sendMessage;
    MessagesService.getMessageById(message.messageid).then(GetMessageResponse).catch(Error);

    vm.closeModal = closeModal;

    function closeModal(){
        $uibModalInstance.dismiss('cancel');
    }


    function sendMessage()
    {
        var data = {otheruserid: message.otherUserId, content: vm.content}
        MessagesService.sendMessage(data).then(sendMessageResponse).catch(Error);
    }


    function sendMessageResponse(res) {
        console.log(res);
        if (res.success === true) {
            vm.updateOk = true;
            vm.message.messages.push(res.data);
            vm.content = "";
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }

    function GetMessageResponse(res) {
        console.log(res);
        if (res.success === true) {

        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }

    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }

}]);