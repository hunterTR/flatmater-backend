/**
 * Created by cem.kaya on 28-Nov-15.
 */

'use strict';
angular.module("app.messages").
controller("MessagesCtrl",['AuthenticationService','MessagesService','$uibModal', function (AuthenticationService,MessagesService,$uibModal) {
    var vm = this;

    MessagesService.getMessages().then(GetMessagesResponse).catch(Error);


    vm.user = AuthenticationService.getCurrentUser();
    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);
    vm.isLoading = true;

    function sendMessage(data)
    {
        MessagesService.sendMessage(data).then().catch();
    }

    function GetMessagesResponse(res) {
        console.log(res);
        if (res.success === true) {

            res.data.forEach(function(message){
                if(message.sendername === vm.user.name + " " + vm.user.lastname )
                {
                    message.name = message.recievername;
                    message.otherUserId = message.recieverid;
                }
                else
                {
                    message.name = message.sendername;
                    message.otherUserId = message.senderid;
                }

            });

            vm.messages = res.data;
            vm.isLoading =false;
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
            vm.isLoading =false;
        }

    }

    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }


    vm.open = function (message) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/messages/messageModal/messageModal.html',
            controller: 'MessageModalCtrl',
            controllerAs:'vm',
            resolve:{
                message: function () {
                    return message;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            console.log($scope.selected);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };


}]);