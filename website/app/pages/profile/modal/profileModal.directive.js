/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.profile')
    .directive("profilePage", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/modal/profileModal.html',
            scope: {
                user: "=",
                Lightbox : "=",
                open:"="
            },
            controller: 'ProfileModalCtrl'
        }
    });

