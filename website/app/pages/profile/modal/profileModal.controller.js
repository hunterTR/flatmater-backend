/**
 * Created by cem.kaya on 29-Oct-15.
 */
'use strict';

angular.module("app.profile")
    .controller("ProfileModalCtrl",['$scope','$timeout','Lightbox','$uibModal','angularGridInstance',
    function ProfileModalCtrl($scope,$timeout,Lightbox,$uibModal,angularGridInstance) {

        var vm = this;
        //$scope.myContent= $sce.trustAsHtml(stuff.content);
        $scope.changeTab = changeTab;
        $scope.personTab = true;
        $scope.houseTab=false;
        $scope.aboutTab = false;
        $scope.test = "testetsetst";
        $scope.selectedTab = "person";


        $scope.Lightbox = Lightbox;
        function changeTab(tab){
           if(tab == "person")
           {
               $scope.selectedTab = 'person';
               $scope.houseTab = false;
               $scope.aboutTab = false;
               $scope.personTab = true;
           }
            else if(tab == "house")
            {
                $scope.selectedTab = 'house';
                $scope.personTab = false;
                $scope.aboutTab = false;
                $scope.houseTab = true;
                $scope.reRenderMap();
            }
            else if(tab == "about")
            {
                $scope.selectedTab = 'about';
                $scope.personTab = false;
                $scope.houseTab = false;
                $scope.aboutTab = true;
            }

        }




        $scope.testfunction = function(){
            console.log('test func');
        }
        $scope.openLightbox = function(photos,index){

            console.log('open light box modal');
        Lightbox.openModal(photos, index);
        }

        //GOOGLE MAPS
        $scope.maps = [];

        $scope.$on('mapInitialized', function(evt, evtMap) {
            $scope.maps.push(evtMap);
        });

        $scope.reRenderMap = function() {   //Rendering map to reinitialize on modals and tabs.
            $timeout(function(){
                angular.forEach($scope.maps, function(index) {
                    console.log("re rendering map");
                    var currCenter = index.getCenter();
                    google.maps.event.trigger(index, 'resize');
                    index.setCenter(currCenter);
                });
                angularGridInstance.gallery2.refresh(); // refreshing housePhotos.
            }, 500);
        };




        $scope.open = function (index) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/profile/edit/photoGallery/photoGallery.html',
                controller:'CarouselDemoCtrl',
                size:'lg',
                resolve: {
                    user: function () {
                        return $scope.user;
                    },
                    index: function(){
                        return index;
                    }
                }
            });
        }

        $scope.openReportModal = function(){
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/components/reportModal/reportModal.html',
                controller: 'ReportModalCtrl',
                controllerAs:'vm',
                resolve:{
                    reporteduser: function () {
                        return $scope.user;;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                console.log($scope.selected);
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        }

    }]);