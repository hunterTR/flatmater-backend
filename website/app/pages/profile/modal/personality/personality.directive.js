/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.profile')
    .directive("personality", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/modal/personality/personality.html',
            scope: {
                user: "="
            },
            controller:'PersonalityModalCtrl',
            controllerAs:'vm'
        }
    });

