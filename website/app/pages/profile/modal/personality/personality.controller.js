'use strict';
angular.module("app.profile").
controller("PersonalityModalCtrl",function($scope,$uibModal){
    var vm = this;

    vm.open = function (index) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/profile/edit/photoGallery/photoGallery.html',
            controller:'CarouselDemoCtrl',
            size:'lg',
            resolve: {
                user: function () {
                    return $scope.user;
                },
                index: function(){
                    return index;
                }
            }
        });
    }

});