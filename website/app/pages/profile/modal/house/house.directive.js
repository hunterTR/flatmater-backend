/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.profile')
    .directive("house", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/modal/house/house.html',
            scope: {
                house: "="
            },
            controller: "HouseModalCtrl",
            controllerAs: "vm"
        }
    });

