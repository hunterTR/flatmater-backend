/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.profile')
    .directive("about", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/modal/about/about.html',
            scope: {
                user: "="
            }
        }
    });

