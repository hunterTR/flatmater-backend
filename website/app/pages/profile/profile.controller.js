/**
 * Created by cem.kaya on 26-Nov-15.
 */
'use strict';

angular.module("app.profile")
    .controller("ProfileCtrl",
        ['ProfileService','$state','$stateParams','$uibModal',function ProfileCtrl(ProfileService,$state,$stateParams,$uibModal) {

            var vm = this;
            //$scope.myContent= $sce.trustAsHtml(stuff.content);
            ProfileService.getUserById($stateParams.name).then(Response).catch(Error);



            vm.openMessageModal = function(user){
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/components/sendMessageModal/sendMessageModal.html',
                    controller: 'SendMessageModalCtrl',
                    controllerAs:'vm',
                    resolve:{
                        user: function () {
                            return user;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                    console.log($scope.selected);
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };


            function Response(res) {
                console.log(res);
                if (res.success === true) {
                  vm.user = res.data;
                }
                else {
                    vm.errorMessage = res.message;
                    vm.error = true;
                    $state.go('home');
                }

            }
            function Error(err) {
                vm.errorMessage = "Problem Occured!";
              //  vm.error = true;
                //$state.go('home');
                console.log(err);
            }

        }]);