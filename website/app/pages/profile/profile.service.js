/**
 * Created by cem.kaya on 26-Nov-15.
 */
'use strict';

angular.module('app.profile')
    .service('ProfileService',['$http', function ($http) {

        var service ={
           getUserById:getUserById
        };

        return service;

        function getUserById(userId){
            var processUrl = "/getuser";

            var data = {userid:userId};
            return $http.post(processUrl,data)
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }

        }



    }]);

