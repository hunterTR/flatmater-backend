/**
 * Created by cem.kaya on 29-Oct-15.
 */

angular.module("app.profile", [
    'ngMessages',
    'textAngular',
    'ui.mask'
]);
