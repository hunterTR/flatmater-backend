/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.edit')
    .directive("photoGallery", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/edit/photoGallery/photoGallery.html',
            scope: {
                vm: "="
            }
        }
    });

