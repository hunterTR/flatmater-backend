/**
 * Created by cem.kaya on 30-Oct-15.
 */
(function () {
    'use strict';

    angular.module("app.edit")
        .controller("EditProfileCtrl",['$scope', '$timeout','$anchorScroll' , 'NgMap', 'Lightbox','angularGridInstance', 'AuthenticationService','$location', 'EditService', 'CookiesService', '$facebook','religiousViews','politicalViews','hobbies','sports','horoscopes','organizations','budgets','propertyTypes','$uibModal',
            function ($scope, $timeout,$anchorScroll , NgMap, Lightbox,angularGridInstance, AuthenticationService,$location, EditService, CookiesService, $facebook,religiousViews,politicalViews,hobbies,sports,horoscopes,organizations,budgets,propertyTypes,$uibModal) {


                var vm = this;
                vm.house = null;
                vm.user = null;
                vm.informations = null;
                vm.user = AuthenticationService.getCurrentUser();
                vm.updateInformation = updateInformation;
                vm.updateHouse = updateHouse;
                vm.updateProfile = updateProfile;
                vm.updateAvatar = updateAvatar;
                vm.isItemExist = isItemExist;
                vm.toggleCheckbox = toggleCheckbox;
                vm.unlistHouse = unlistHouse;
                vm.onDistrictChange = onDistrictChange;
                vm.onCityChange = onCityChange;
                vm.onDistrictChangeForInformations = onDistrictChangeForInformations;
                vm.onCityChangeForInformations = onCityChangeForInformations;
                vm.onMapDrag = onMapDrag;
                vm.onAddressChange = onAddressChange;
                vm.shareProfileLink = shareProfileLink;
                vm.scrollToTop = scrollToTop;
                vm.saveHobbies = saveHobbies;
                vm.saveSports = saveSports;
                vm.Lightbox = Lightbox;   // for thumbnail and image modals.
                vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);
                vm.updateMessage = "";
                vm.personalUpdateOk = false;
                vm.houseUpdateOk = false;
                vm.informationsUpdateOk = false;
                vm.housePhotos = [];
                vm.profilePhotos = [];
                vm.hobbiesSaved = false;
                vm.sportsSaved = false;

                vm.propertyTypes = propertyTypes;
                vm.numbers = [1, 2, 3, 4, 5, 6, 7];
                vm.religions = religiousViews;
                vm.politics = politicalViews;
                vm.organizations = organizations;
                vm.budgets = budgets;

                vm.sports =sports;

                vm.hobbies =hobbies;

                vm.personalPhotos = [
                    {
                        tooltip: "People who loves video games",
                        img: "img/portfolio/gamer.jpg"
                    },
                    {
                        tooltip: "People who loves video music",
                        img: "img/portfolio/musician.jpg"
                    },
                    {
                        tooltip: "People who loves pets",
                        img: "img/portfolio/petowner.jpg"
                    },
                    {
                        tooltip: "People who loves parties",
                        img: "img/portfolio/partylover.jpg"
                    },
                    {
                        tooltip: "People who loves to make food",
                        img: "img/portfolio/chef.jpg"
                    },
                    {
                        tooltip: "People who smokes occasionally",
                        img: "img/portfolio/smoker.jpg"
                    },
                    {
                        tooltip: "People who supports LGBT",
                        img: "img/portfolio/gayfriendly.jpg"
                    },
                    {
                        tooltip: "People who loves video games",
                        img: "img/portfolio/traveler.jpg"
                    },
                    {
                        tooltip: "People who loves sports",
                        img: "img/portfolio/sport.jpg"
                    }, {
                        tooltip: "People who loves to stay all day up",
                        img: "img/portfolio/owl.jpg"
                    },
                    {
                        tooltip: "People who loves to be clean",
                        img: "img/portfolio/temiz.jpg"
                    },
                    {
                        tooltip: "People who are messy",
                        img: "img/portfolio/messy.jpg"
                    }

                ];


                vm.horoscopes = ["Aries", "Taurus", "Gemini", "Cancer", "Leo", "Libra",
                    "Scorpio", "Sagittarius", "Capicorn", "Aquarius", "Pisces"];

                vm.countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];

                vm.resizeMap = function () {
                    $scope.reRenderMap();
                }

                $scope.reRenderMap = function () {
                    $timeout(function () {
                        angular.forEach($scope.maps, function (index) {
                            console.log("re rendering map");
                            var currCenter = index.getCenter();
                            google.maps.event.trigger(index, 'resize');
                            index.setCenter(currCenter);

                        });
                        angularGridInstance.gallery2.refresh(); // refreshing housePhotos.
                    }, 500);
                }

                $scope.maps = [];

                $scope.$on('mapInitialized', function (evt, evtMap) {
                    $scope.maps.push(evtMap);
                });

                //For multiple selection parts..
                function isItemExist(item, list) {
                    return list.indexOf(item) > -1;
                }

                function toggleCheckbox(item, list) {
                    var idx = list.indexOf(item);
                    if (idx > -1) list.splice(idx, 1);
                    else list.push(item);

                    console.log(vm.user.personalityphotos);
                }

                ////////
                function onCityChange() {
                    var place = this.getPlace();
                    console.log('seçildi');
                    console.log(place.name);
                    vm.house.location.city = place.name;
                    //   $scope.reRenderMap();
                }

                function onDistrictChange() {
                    var place = this.getPlace();
                    console.log('seçildi');
                    console.log(place.name);
                    vm.house.location.district = place.name;
                }

                function onCityChangeForInformations() {
                    var place = this.getPlace();
                    console.log('seçildi');
                    vm.informations.city = place.name;
                    //   $scope.reRenderMap();
                }

                function onDistrictChangeForInformations() {
                    var place = this.getPlace();
                    console.log('seçildi');
                    vm.informations.district = place.name;
                }

                function onAddressChange() {   // for house
                    console.log("adres değişti");

                    console.log(vm.marker);
                    console.log(vm.map.getCenter().lng());
                    vm.house.location.latitude = vm.map.getCenter().lat();
                    vm.house.location.longitude = vm.map.getCenter().lng();
                }

                function saveHobbies() {
                    vm.hobbiesSaved = true;
                    vm.saveMessage = "register_modal_hobbies";
                }

                function saveSports() {
                    vm.sportsSaved = true;
                    vm.saveMessage = "register_modal_sports";

                }

                NgMap.getMap().then(function (map) {
                    vm.map = map;
                    vm.marker = new google.maps.Marker({title: "Marker"});

                    if (vm.user.house != null && vm.user.house != undefined)  // if house exist.
                    {
                        if (vm.user.house.location.latitude != undefined && vm.user.house.location.latitude != null &&
                            vm.user.house.location.longitude != undefined && vm.user.house.location.longitude != null) {

                            vm.mapCenter = [vm.house.location.latitude, vm.house.location.longitude];
                            //   vm.map.setCenter(latlng);
                        }
                        else {
                            vm.mapCenter = vm.house.location.address;
                            console.log("latitude longitude yok.");
                        }
                        var latlng = new google.maps.LatLng(vm.house.location.latitude, vm.house.location.longitude);
                        vm.marker.setPosition(latlng);
                    } else {
                        console.log("house information yok");
                        vm.mapCenter = "current-location";
                    }


                    vm.marker.setMap(map);
                });

                function onMapDrag() {
                    vm.marker.setPosition(vm.map.getCenter());
                    vm.house.location.latitude = vm.map.getCenter().lat();
                    vm.house.location.longitude = vm.map.getCenter().lng();
                }

                vm.user.birthdate = new Date(vm.user.birthdate);


                if (vm.user.house != null && vm.user.house != undefined)  // if house exist.
                {
                    vm.house = vm.user.house;
                    vm.housePhotos = vm.user.house.photos;
                    vm.house.availabledate = new Date(vm.house.availabledate);
                    // console.log(vm.house);
                }
                else {
                    vm.house = {location: {}};
                }


                if (vm.user.informations != null && vm.user.informations != undefined)  // if informations exist.
                {
                    vm.informations = vm.user.informations;
                }
                else {
                    vm.informations = null;
                }


                /// ALERT CHECKs

                if (vm.user.photos.length > 0) {
                    vm.isprofilephotosexist = true;
                }
                else {
                    vm.isprofilephotosexist = false;
                }

                if (vm.housePhotos.length > 0) {
                    vm.ishousephotosexist = true;
                }
                else {
                    vm.ishousephotosexist = false;
                }

                if (vm.user.selfdescription != null) {
                    vm.isselfdescriptionexist = true;
                }
                else {
                    vm.isselfdescriptionexist = false;
                }


                function updateInformation(infos) {
                    EditService.updateInformations(infos)
                        .then(Response)
                        .catch(Error);
                }

                function updateHouse(house) {
                    EditService.updateHouse(house)
                        .then(UpdateHouseResponse)
                        .catch(Error);
                }

                function updateProfile(user) {
                    EditService.updateProfile(user)
                        .then(Response)
                        .catch(Error);
                }

                function updateAvatar() {

                }

                function unlistHouse() {
                    vm.house = null;
                    vm.user.house = null;
                    EditService.unlistHouse()
                        .then(Response)
                        .catch(Error);

                }

                function Response(res) {
                    console.log(res);
                    if (res.success === true) {
                        vm.updateOk = true;
                        vm.scrollToTop();
                        vm.updateMessage = res.message;
                        showResponseTextModal(res.message);
                    }
                    else {
                        vm.errorMessage = res.message;
                        vm.error = true;
                    }

                }

                function UpdateHouseResponse(res) {
                    console.log(res);
                    if (res.success === true) {
                        vm.user.house = vm.house;
                        vm.updateOk = true;
                        vm.scrollToTop();
                        vm.updateMessage = res.message;
                        showResponseTextModal(res.message);
                    }
                    else {
                        vm.errorMessage = res.message;
                        vm.error = true;
                    }

                }

                function Error(err) {
                    console.log(err);
                }

                function shareProfileLink() {
                    $facebook.ui({
                        method: 'feed',
                        link: 'theflatmater.com/profile/' + vm.user.userid,
                        caption: 'An example caption',
                    });
                }

                function scrollToTop(){
                    $location.hash("ScrollTop");

                    $anchorScroll()

                    $location.hash('');
                }


                vm.UserPhotosEventHandlers = {
                    'addedfile': function (file) {

                    },

                    'success': function (file, response) {
                        console.log(response);
                        if (response.success === true) {
                            console.log("yeeeee");
                            vm.user.photos.push(response.link);
                            vm.profilePhotos = vm.user.photos;
                        } else {
                        }
                    }

                };
                vm.HousePhotosEventHandlers = {
                    'addedfile': function (file) {

                    },

                    'success': function (file, response) {
                        console.log(response);
                        if (response.success === true) {
                            vm.house.photos.push(response.link);
                            vm.housePhotos = vm.house.photos;
                        } else {
                        }
                    }

                };

                vm.AvatarEventHandlers = {
                    'addedfile': function (file) {

                    },

                    'success': function (file, response) {
                        console.log(response);
                        if (response.success === true) {
                           vm.user.useravatar = response.link;
                        } else {
                        }
                    }

                };

                var closeModal =function (){
                    setTimeout(function(){ $scope.$apply(); });
                };
                vm.avatarDropzoneconfig = {
                    url: '/api/uploadavatar',
                    headers: {'x-access-token': CookiesService.getFromCookies('token')},
                    maxFilesize: 1,
                    maxThumbnailFilesize: 1,
                    maxFiles: 1,
                    paramName: "ProfilePhotoFile",
                    acceptedFiles: '.jpg,.jpeg,.png,.gif',
                    dictInvalidFileType: "Unsupported file type!",
                    parallelUploads: 1,
                };

                vm.houseDropzoneconfig = {
                    url: '/api/uploadphotoforhouse',
                    headers: {'x-access-token': CookiesService.getFromCookies('token')},
                    maxFilesize: 1,
                    maxThumbnailFilesize: 10,
                    maxFiles: 10,
                    paramName: "ProfilePhotoFile",
                    acceptedFiles: '.jpg,.jpeg,.png,.gif',
                    dictInvalidFileType: "Unsupported file type!",
                    parallelUploads: 1,
                };

                vm.profileDropzoneconfig = {
                    url: '/api/uploadphotoforprofile',
                    headers: {'x-access-token': CookiesService.getFromCookies('token')},
                    maxFilesize: 1,
                    maxThumbnailFilesize: 5,
                    maxFiles: 5,
                    paramName: "ProfilePhotoFile",
                    acceptedFiles: '.jpg,.jpeg,.png,.gif',
                    dictInvalidFileType: "Unsupported file type!",
                    parallelUploads: 1,
                };


                vm.housePhotoUploadSettings = {
                    modalTitle: "Upload Photos For Your House",
                    dropzoneconfig: vm.houseDropzoneconfig,
                    id: "housePhotoUpload",
                    eventHandlers: vm.HousePhotosEventHandlers,
                    close: closeModal
                };

                vm.profilePhotoUploadSettings = {
                    modalTitle: "Upload Photos For Your Profile",
                    dropzoneconfig: vm.profileDropzoneconfig,
                    eventHandlers: vm.UserPhotosEventHandlers,
                    id: "profilePhotoUpload",
                    close: closeModal
                };


                vm.avatarUploadSettings = {
                    modalTitle: "Upload Photos For Your Profile",
                    dropzoneconfig: vm.avatarDropzoneconfig,
                    eventHandlers: vm.AvatarEventHandlers,
                    id: "updateAvatarModal",
                    close: closeModal
                };



                vm.profilePhotos = vm.user.photos;

                $scope.deletePhoto = function (url) {
                    console.log("delete");
                    EditService.deletePhoto(url).then(function (res) {

                        var userphotosindex = vm.user.photos.indexOf(url);
                        var housephotosindex = vm.house.photos.indexOf(url);
                        if (userphotosindex > -1) {
                            vm.user.photos.splice(userphotosindex, 1);
                            vm.profilePhotos = vm.user.photos;
                            Lightbox.closeModal();
                        }
                        if (housephotosindex > -1) {
                            vm.house.photos.splice(housephotosindex, 1);
                            vm.housePhotos = vm.house.photos;
                            Lightbox.closeModal();
                        }

                    }).catch(Error);

                };

                function showResponseTextModal(responseText){
                    var textfor = {"title" : "UPDATE", "textToShow" : responseText};

                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'app/components/textShowModal/textShowModal.html',
                        controller: 'TextShowModalCtrl',
                        controllerAs: 'vm',
                        resolve: {
                            text: function () {
                                return textfor;
                            }
                        }
                    });
                }

            }]);


})();