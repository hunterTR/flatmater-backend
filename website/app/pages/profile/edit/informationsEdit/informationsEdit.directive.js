/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.edit')
    .directive("informationsEdit", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/edit/informationsEdit/informationsEdit.html',
            scope: {
                vm: "="
            }
        }
    });
