/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.edit')
    .directive("houseEdit", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/edit/houseEdit/houseEdit.html',
            scope: {
                vm: "=",
            },
            controller:'HouseEditCtrl'
        }
    });

