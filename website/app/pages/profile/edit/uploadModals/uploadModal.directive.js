/**
 * Created by cem.kaya on 02-Nov-15.
 */
'use strict'
angular.module('app.edit')
    .directive("uploadModal", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/edit/uploadModals/uploadModal.html',
            scope: {
                settings: "="
            }
        }
    });

