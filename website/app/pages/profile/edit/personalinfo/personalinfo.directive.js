/**
 * Created by cem.kaya on 29-Oct-15.
 */
angular.module('app.edit')
    .directive("personalInfo", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl:'app/pages/profile/edit/personalinfo/personalinfo.html',
            scope: {
                vm: "="
            }
        }
    });

