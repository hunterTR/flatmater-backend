'use strict';

angular.module('app.edit')
    .service('EditService',['$http','CookiesService', function ($http,CookiesService) {

        var service ={
            updateProfile: updateProfile,
            updateHouse: updateHouse,
            updateInformations: updateInformations,
            updateAvatar: updateAvatar,
            deletePhoto: deletePhoto,
            unlistHouse : unlistHouse
        };


        return service;

        function updateInformations(informations){
            var processUrl = "/api/updateinformation";

            return $http.post(processUrl,informations,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }

        }

        function updateHouse(house){
            var processUrl = "/api/updatehouse";

            return $http.post(processUrl,house,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }
        }

        function unlistHouse()
        {
            var processUrl = "/api/unlisthouse";

            return $http.post(processUrl,{},{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }

        }

        function updateProfile(user){
            var processUrl = "/api/updateprofile";

            return $http.post(processUrl,user,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }
        }

        function updateAvatar(file){
                var fd = new FormData();
                fd.append('file', file);
                $http.post('/api/uploadphotoforprofile', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                    .success(function(){
                    })
                    .error(function(){
                    });
            }

        function deletePhoto(imageurl){
            var processUrl = "/api/deletephoto";

            return $http.post(processUrl,{'imageurl':imageurl},{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }
        }




    }]);

