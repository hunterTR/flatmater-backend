'use strict';

angular.module('app.settings')
    .service('SettingsService',['$http','CookiesService', function ($http,CookiesService) {

        var service ={
            changePassword:changePassword,
            deleteAccount:deleteAccount,
            sendVerificationMail : sendVerificationMail,

        };


        return service;

        function changePassword(data){
            var processUrl = "/api/changepassword";

            return $http.post(processUrl,data,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }

        }

        function deleteAccount(data){
            var processUrl = "/api/deleteaccount";

            return $http.post(processUrl,data,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }
        }

        function sendVerificationMail(){
            var processUrl = "/api/sendnewverificationemail";

            return $http.post(processUrl,{},{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }
        }

    }]);

