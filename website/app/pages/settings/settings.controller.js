'use strict';
angular.module("app.settings").
controller("SettingsCtrl",['AuthenticationService','SettingsService','$window','md5',function(AuthenticationService,SettingsService,$window,md5){
    var vm = this;
    vm.changePassword = changePassword;
    vm.deleteAccount = deleteAccount;
    vm.sendVerificationMail = sendVerificationMail;
    vm.user = AuthenticationService.getCurrentUser();
    vm.oldPassword = "";
    vm.newPassword = "";
    vm.updateOk= false;
    vm.error = false;
    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);

    function changePassword()
    {
        var data = {oldPassword : md5.createHash(vm.oldPassword), newPassword: md5.createHash(vm.newPassword)};
        SettingsService.changePassword(data).then(Response)
            .catch(Error);
    }

    function deleteAccount()
    {
        SettingsService.deleteAccount({}).then(DeleteResponse)
            .catch(Error);
    }

    function sendVerificationMail () {
        SettingsService.sendVerificationMail().then(Response)
            .catch(Error);
    }


    function Response(res) {
        console.log(res);
        if (res.success === true) {
            vm.updateOk = true;
            vm.updateMessage = res.message;
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }
    function DeleteResponse(res) {
        console.log(res);
        if (res.success === true) {

            AuthenticationService.logout();
            $window.location.reload();
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }



    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }
    
}]);