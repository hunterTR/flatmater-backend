/**
 * Created by cem on 08/11/15.
 */
angular.module('app.listing')
    .directive("searchModal", function () {
        return{
            restrict: 'E',
            replace:true,
            templateUrl: 'app/pages/listing/searchModal/searchModal.html',
            scope:{
                vm:"="
            }
        }

    });