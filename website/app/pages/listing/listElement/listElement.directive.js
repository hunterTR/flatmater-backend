/**
 * Created by cem.kaya on 28-Oct-15.
 */

angular.module('app.listing')
    .directive("listElement", function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/pages/listing/listElement/listElement.html',
            scope: {
                user: "=",
                listingPlace: "=",
                vm : "="
            },
            controller:'ListElementCtrl'
        }

    });