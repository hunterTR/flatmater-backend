/**
 * Created by cem.kaya on 28-Oct-15.
 */

angular.module('app.listing')
    .directive("listModal", function () {
        return{
            restrict: 'E',
            replace:true,
            templateUrl: 'app/pages/listing/listElement/listModal.html',
            scope:{
                user:"="
            }
        }

    });