'use strict';

angular.module('app.listing')
    .service('ListingService',['$http','CookiesService', function ($http,CookiesService) {

   var service ={
       findMatches : findMatches,
       strictSearch : strictSearch
   };

        function findMatches(user,skip,limit)
        {
            var processUrl = "/api/findmatches";

            user.skip = skip;
            user.limit = limit;
            return $http.post(processUrl,user,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }

            function error(err) {
            }
        }

        function strictSearch(user,skip,limit){
            var processUrl = "/api/strictfindmatches";

            user.skip = skip;
            user.limit = limit;

            return $http.post(processUrl,user,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }
        }

        return service;

    }]);
