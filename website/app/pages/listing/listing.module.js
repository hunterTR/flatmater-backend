/**
 * Created by cem.kaya on 28-Oct-15.
 */
'use strict'
angular.module("app.listing", [
    'app.profile',
    'infinite-scroll'
]);