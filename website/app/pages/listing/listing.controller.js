/**
 * Created by cem.kaya on 28-Oct-15.
 */
(function () {
    'use strict';

    angular.module("app.listing")
        .controller("ListingCtrl",['AuthenticationService', 'ListingService', '$uibModal', '$window','religiousViews','politicalViews','horoscopes','hobbies','sports','budgets','organizations'
            ,function (AuthenticationService, ListingService, $uibModal, $window,religiousViews,politicalViews,horoscopes,hobbies,sports,budgets,organizations) {

                var vm = this;


                vm.isSearchMenuOpen = false;
                vm.toggleMenu = toggleMenu;
                vm.manualSearch = manualSearch;
                vm.addMoreItems = addMoreItems;
                vm.errorMessage = null;
                vm.user = angular.copy(AuthenticationService.getCurrentUser());
                vm.informations = vm.user.informations;
                vm.strictSearch = false;

                console.log("POLİTİİİK ", politicalViews);
                vm.isLoading = false;
                vm.fullyLoaded = false;
                vm.users =[];   // bu eklenmediginde alttaki listing service çağrılması gerekiyor. fakat bu ekledniğinde otomatik yapıyoruz. düzgün test edilmesi gerekiyor.

                //
                //ListingService.findMatches(vm.user, 0, 6)
                //    .then(matchResponse)
                //    .catch(matchError);


                vm.religions = religiousViews;

                vm.politics = politicalViews;

                vm.organizations = organizations;

                vm.budgets = budgets;

                vm.horoscopes = horoscopes;

                vm.sports = sports;

                vm.hobbies = hobbies;


                function matchResponse(res) {
                    console.log(res);
                    if (res.success === true) {
                        vm.users = res.data;
                        vm.isLoading = false;
                    }
                    else {
                        vm.errorMessage = res.message;
                        vm.users = [];
                        if (vm.errorMessage === "Failed to authenticate token.") {
                            $window.location.reload();
                        }

                    }

                }

                function moreItemResponse(res) {
                    console.log(res);
                    if (res.success === true) {
                        var datalength = res.data.length;

                        if (datalength !== 0) {
                            for (var i = 0; i < datalength; i++) {
                                vm.users.push(res.data[i]);
                                if (i >= res.data.length - 1) {
                                    vm.isLoading = false;
                                }
                            }
                        }
                        else {
                            vm.isLoading = false;
                            vm.fullyLoaded = true;
                            console.log('fully loaded');
                        }

                    }
                    else {

                        vm.errorMessage = res.message;
                        vm.isLoading = false;
                    }

                }

                function matchError(err) {
                    console.log(err);
                }


                function toggleMenu() {
                    vm.isSearchMenuOpen = !vm.isSearchMenuOpen;
                }

                function manualSearch() {
                    if (vm.strictSearch === true) {
                        vm.users = null;
                        vm.isLoading = true;
                        console.log('strict search');
                        ListingService.strictSearch(vm.user, 0, 6)
                            .then(matchResponse)
                            .catch(matchError);

                        vm.fullyLoaded = false;
                    }
                    else {
                        vm.users = null;
                        vm.isLoading = true;
                        console.log('normal search');
                        ListingService.findMatches(vm.user, 0, 6)
                            .then(matchResponse)
                            .catch(matchError);

                        vm.fullyLoaded = false;
                    }

                }

                function addMoreItems() {
                    if (vm.strictSearch === true) {
                        if (vm.users != null && vm.users != undefined && vm.isLoading != true && vm.fullyLoaded != true) {
                            console.log('add add ' + vm.users.length);

                            vm.isLoading = true;
                            ListingService.strictSearch(vm.user, vm.users.length, 6)
                                .then(moreItemResponse)
                                .catch(matchError);
                        }
                        else {
                            console.log("loading");
                        }
                    }
                    else {
                        if (vm.users != null && vm.users != undefined && vm.isLoading != true && vm.fullyLoaded != true) {
                            console.log('add add ' + vm.users.length);

                            vm.isLoading = true;
                          vm.myPromise = ListingService.findMatches(vm.user, vm.users.length, 6)
                                .then(moreItemResponse)
                                .catch(matchError);
                        }
                        else {
                            console.log("loading");
                        }
                    }

                }


                vm.open = function (user) {

                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'app/pages/listing/listElement/listModal.html',
                        controller: 'ModalInstanceCtrl',
                        resolve: {
                            user: function () {
                                return user;
                            }
                        }

                    });

                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                        console.log($scope.selected);
                    }, function () {
                        //$log.info('Modal dismissed at: ' + new Date());
                    });
                };


            }]);


})();