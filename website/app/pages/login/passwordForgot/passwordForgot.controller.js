/**
 * Created by cem.kaya on 19-Nov-15.
 */

angular.module('app.login').controller('PasswordForgotCtrl',['$scope','$uibModalInstance','AuthenticationService','CookiesService','RandomStringService', function ($scope,$uibModalInstance,AuthenticationService,CookiesService,RandomStringService) {

    $scope.isOK=false;
    $scope.responseMessage = "";
    $scope.sendRecoverPassword = sendRecoverPassword;

    $scope.isLoading = false;

    function sendRecoverPassword() {
        $scope.isLoading = true;
        var clientpasswordtoken = RandomStringService.randomString(18);
        CookiesService.saveToCookies("_cpt",clientpasswordtoken,1);
        var datas = {
            email: $scope.email,
            clientPasswordToken: clientpasswordtoken
        };
        AuthenticationService.recoverPassword(datas,function (data) {


            if (data.success === true) {
                console.log(data);
                $scope.isOK = true;
                $scope.responseMessage = data.message;
                $scope.isLoading = false;
            }
            else {
                $scope.isOK = false;
                $scope.responseMessage = data.message;
                $scope.isLoading = false;
            }
        });
    }


    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);