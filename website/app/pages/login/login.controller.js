/**
 * Created by cem.kaya on 26-Oct-15.
 */
'use strict';
angular.module("app.login").
controller("LoginCtrl", ['AuthenticationService','$facebook','$uibModal','$window','$location','md5',function (AuthenticationService,$facebook,$uibModal,$window,$location,md5) {
    var vm = this;
    vm.indicator = false;
    vm.login = login;
    vm.fbLogin = fbLogin;
    vm.isLoading = false;

    vm.email;
    vm.password;
    vm.responseMessage = "";
    vm.isOK = false;
    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);
    vm.error = false;

    if (AuthenticationService.getCurrentUser().success === true)   // daha iyi bir yere ta��nacak.
    {
        $location.path("/");
    }



    function login() {
        vm.isLoading = true;
        vm.error = false;

        AuthenticationService.login(vm.email, md5.createHash(vm.password), function (data) {
            vm.indicator = false;

            if (data.success === true) {
                console.log(data);
                $window.location.reload();
                vm.isLoading = false;

            }
            else {
                vm.error = true;
                vm.isLoading = false;
            }
        });
    }

    function fbLogin() {
        vm.isLoading = true;
        // console.log ($facebook.getVersion());
        console.log("fb login clicked");
        $facebook.login().then(function() {
            console.log($facebook.getAuthResponse());
            AuthenticationService.fbLogin($facebook.getAuthResponse(),function (data) {
                vm.indicator = false;

                if (data.success === true) {
                    console.log(data);
                    $window.location.reload();
                    vm.isLoading = false;

                }
                else {
                    vm.error = true;
                    vm.isLoading = false;

                }
            });

        });
    }




    vm.openPfModal = function (user) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/login/passwordForgot/passwordForgot.html',
            controller: 'PasswordForgotCtrl',
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            console.log($scope.selected);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };


}]);