'use strict';
angular.module("app.navbar").
controller("NavbarCtrl",['$state','ApplicationService','AuthenticationService','$window','$location','$anchorScroll','MessagesService','$uibModal',function($state,ApplicationService,AuthenticationService,$window,$location,$anchorScroll,MessagesService,$uibModal){
    var vm = this;

    vm.logout = logout;
    vm.resetNotificationCount = resetNotificationCount;

    MessagesService.checkNotifications().then(GetNotificationsResponse).catch(Error);


    vm.navigate = function(state,hash){
        $state.go(state);
        $location.hash(hash);
    }

    ApplicationService.registerListener(function () {
        // when app is ready, redirect by case
        console.log('test');
        if (AuthenticationService.getCurrentUser().success != null && AuthenticationService.getCurrentUser().success != undefined  && AuthenticationService.getCurrentUser().success === true ) {
            vm.isLoggedIn = true;
            vm.user = AuthenticationService.getCurrentUser();
        } else {

        }
    });


  function logout() {
      AuthenticationService.logout();
      $window.location.reload();
  }

    vm.scrollToX = function(target)
    {
       // $location.hash('');
        $location.path("/").hash(target);
        console.log("scroll to x");
        // call $anchorScroll()
        $anchorScroll();
        $location.hash('');

    }

    function GetNotificationsResponse(res) {
        console.log(res);
        if (res.success === true) {
            vm.messageNotificationsCount = res.data;
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }

    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }

    function resetNotificationCount(){
        vm.messageNotificationsCount = 0;
    }




}]);