/**
 * Created by cem.kaya on 06-Dec-15.
 */
'use strict';

angular.module("app.password").
controller("PasswordCtrl",['$state', '$stateParams','PasswordService', 'CookiesService','md5', function ($state, $stateParams, PasswordService, CookiesService,md5) {
    var vm = this;

    vm.isVerified = false;
    vm.resetPassword = resetPassword;
    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);


    function resetPassword() {
        var datas = {
            clientPasswordToken: CookiesService.getFromCookies("_cpt"),
            serverPasswordToken: $stateParams.name,
            password: md5.createHash(vm.newPassword)
        };
        PasswordService.resetPassword(datas).then(Response).catch(error);
    }


    function Response(res) {
        console.log(res);
        vm.isVerified = res.success;
        if (res.success === true) {
            CookiesService.saveToCookies("_cpt",null);
            vm.updateOk = true;
            vm.updateMessage = res.message;
        }
        else {
            vm.updateOk = true;
            vm.updateMessage = res.message;
            vm.error = true;
        }

    }


    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }

}]);