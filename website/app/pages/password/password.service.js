/**
 * Created by cem.kaya on 03-Dec-15.
 */
'use strict';

angular.module('app.password')
    .service('PasswordService',['$http', 'CookiesService', function ($http, CookiesService) {

        var service = {
            resetPassword : resetPassword
        };


        return service;

        function resetPassword(data) {
            var processUrl = "/resetpassword";

            return $http.post(processUrl, data)
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }

        }



    }]);

