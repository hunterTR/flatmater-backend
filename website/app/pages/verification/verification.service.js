/**
 * Created by cem.kaya on 03-Dec-15.
 */
'use strict';

angular.module('app.verification')
    .service('VerificationService',['$http','CookiesService', function ($http, CookiesService) {

        var service = {
            verifyEmail: verifyEmail,
            sendNewEmail: sendNewEmail
        };


        return service;

        function verifyEmail(data) {
            var processUrl = "/api/verifyemail";

            return $http.post(processUrl, {emailverifytoken: data}, {headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }

        }

        function sendNewEmail() {
            var processUrl = "/api/sendnewverificationemail";

            return $http.post(processUrl, {}, {headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);
            function response(res) {
                return res.data;
            }
            function error(err) {
            }

        }


    }]);

