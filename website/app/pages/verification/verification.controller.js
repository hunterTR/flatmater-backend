/**
 * Created by cem.kaya on 03-Dec-15.
 */
'use strict';

angular.module("app.verification").
controller("VerificationCtrl",['$state','$stateParams','VerificationService','AuthenticationService',function($state,$stateParams,VerificationService,AuthenticationService){
    var vm = this;

    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);
    vm.isVerified = false;
    vm.sendNewEmail = sendNewEmail;

    vm.user = AuthenticationService.getCurrentUser();

    if(vm.user.status == 'verified')
    {
        $state.go('home');
    }
    else
    {
        VerificationService.verifyEmail($stateParams.name).then(Response).catch(Error);
    }


    function sendNewEmail(){
        VerificationService.sendNewEmail().then(Response).catch(Error);
    }


    function Response(res) {
        console.log(res);
        vm.isVerified = res.success;
        if (res.success === true) {
            vm.updateOk = true;
            vm.updateMessage = res.message;
        }
        else {
            vm.updateOk = true;
            vm.updateMessage = res.message;
            vm.error = true;
        }

    }


    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }

}]);