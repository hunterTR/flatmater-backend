/**
 * Created by cem.kaya on 27-Oct-15.
 */
'use strict';

angular.module("app.loading")
    .controller("LoadingCtrl",['$rootScope','$scope','$state','$stateParams', 'ApplicationService', 'AuthenticationService', '$location',
    function LoadingCtrl($rootScope,$scope,$state,$stateParams, ApplicationService, AuthenticationService, $location) {
        $rootScope.blurClass = 'blur';
        $rootScope.hideClass = 'hide';

        $scope.funny = ["Bulaşıkları yıkadın mı ?","Evi temizledin mi ?","Alışveriş sırası sende"]

        $scope.random = Math.floor((Math.random() * $scope.funny.length));

        ApplicationService.registerListener(function () {
            // when app is ready, redirect by case
            if (AuthenticationService.getCurrentUser().success != null && AuthenticationService.getCurrentUser().success != undefined  && AuthenticationService.getCurrentUser().success === true ) {
                console.log($stateParams);
                console.log("Logged in user exist");
                $state.go($stateParams.stateName,{name:$stateParams.stateParams});
                //$location.path("/listing");
            } else {
                console.log($stateParams);
                console.log("Logged in user doesn't exist");
                $state.go($stateParams.stateName,{name:$stateParams.stateParams});
                //$location.path("/");
            }

            $rootScope.blurClass = '';
            $rootScope.hideClass = '';
        });


}]);