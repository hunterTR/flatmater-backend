'use strict';

angular.module('app.category')
    .service('CategoryService',['$http', function ($http) {

        var service ={
            listByCategory : listByCategory
        };

        function listByCategory(selection,skip,limit)
        {
            var processUrl = "/listbycategory";
            selection.skip = skip;
            selection.limit = limit;

            return $http.post(processUrl,selection)
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;

            }
            function error(err) {
            }
        }

        return service;

    }]);
