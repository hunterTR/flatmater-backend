/**
 * Created by cem.kaya on 07-Nov-15.
 */
(function () {
'use strict';

angular.module("app.category")
    .controller("CategoryCtrl",['$stateParams','AuthenticationService','InformationService','CategoryService','$uibModal','$log',
        function($stateParams,AuthenticationService,InformationService,CategoryService,$uibModal,$log) {

            var vm = this;
            vm.toggleMenu = toggleMenu;
            vm.addMoreItems = addMoreItems;
            vm.errorMessage = null;
            vm.listingPlace = "category";
            vm.isLoading = false;
            vm.fullyLoaded = false;
            vm.users = [];


            console.log($stateParams);

            //CategoryService.listByCategory({selection:'img/portfolio/'+$stateParams.name+'.jpg'},0,6)
            //    .then(matchResponse)
            //    .catch(matchError);

            function matchResponse(res) {
                console.log(res);
                if (res.success === true) {
                    vm.users = res.data;
                }
                else {
                    vm.users = [];
                    vm.errorMessage = res.message;

                }

            }

            function moreItemResponse(res) {
                console.log(res);
                if (res.success === true) {
                    var datalength = res.data.length;

                    if (datalength !== 0) {
                        for (var i = 0; i < datalength; i++) {
                            vm.users.push(res.data[i]);
                            if (i >= res.data.length - 1) {
                                vm.isLoading = false;
                            }
                        }
                    }
                    else {
                        vm.isLoading = false;
                        vm.fullyLoaded = true;
                        console.log('fully loaded');
                    }

                }
                else {

                    vm.errorMessage = res.message;
                    vm.isLoading = false;
                    vm.fullyLoaded = true;
                }

            }

            function matchError(err) {
                console.log(err);
            }


            function toggleMenu() {
                vm.isSearchMenuOpen = !vm.isSearchMenuOpen;
            }

            function addMoreItems() {

                    if (vm.users != null && vm.users != undefined && vm.isLoading != true && vm.fullyLoaded !=true) {
                        console.log('add add ' + vm.users.length);

                        vm.isLoading = true;
                        CategoryService.listByCategory({selection:'img/portfolio/'+$stateParams.name+'.jpg'}, vm.users.length, 6)
                            .then(moreItemResponse)
                            .catch(matchError);
                    }
                    else {
                        console.log("loading");
                    }


            }


            //Listing modal
            vm.open = function (user) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/listing/listElement/listModal.html',
                    controller: 'ModalInstanceCtrl',
                    resolve:{
                        user: function () {
                            return user;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                    console.log($scope.selected);
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };

        }]);


})();