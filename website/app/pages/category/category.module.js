/**
 * Created by cem.kaya on 07-Nov-15.
 */
'use strict'
angular.module("app.category", [
    'app.profile',
    'ngAnimate',
    'ui.bootstrap'
]);