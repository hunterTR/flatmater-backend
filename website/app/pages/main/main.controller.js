/**
 * Created by cem.kaya on 27-Oct-15.
 */
'use strict';
angular.module("app.main").
controller("MainCtrl",['AuthenticationService','InformationService','$location','$state','$timeout','$stateParams','$anchorScroll','$facebook','$window','anchorSmoothScroll', function (AuthenticationService,InformationService,$location,$state,$timeout,$stateParams,$anchorScroll,$facebook,$window,anchorSmoothScroll) {
    var vm = this;
    vm.indicator = false;
    vm.test = "test";
    vm.userPriorities = null;
    vm.setCategorySelection = setCategorySelection;
    vm.nextRegister = nextRegister;
    vm.fbLogin = fbLogin;
    vm.isLoggedIn = false;

    vm.backgroundImageIndex = Math.floor((Math.random() * 4) + 1);
    if($stateParams.name !=null && $stateParams.name != undefined)  // scrolling
    {
            // set the location.hash to the id of
            // the element you wish to scroll to.
        $location.hash($stateParams.name);

            // call $anchorScroll()
        //$anchorScroll();
        $timeout(function(){
            anchorSmoothScroll.scrollTo($stateParams.name);
        },500);

        $location.hash('');
    }

    if (AuthenticationService.getCurrentUser().success != null && AuthenticationService.getCurrentUser().success != undefined  && AuthenticationService.getCurrentUser().success === true ) {
        vm.isLoggedIn = true;
    } else {

    }

    //Register next button click
    function nextRegister() {
        console.log(vm.userPriorities);
        InformationService.setPriorities(vm.userPriorities);
        $state.go('register');
    };

    function setCategorySelection(data) {
        InformationService.setCategorySelection(data);
    }

    vm.scrollToX = function (target) {
        $location.hash(target);

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo(target);
        $location.hash('');
    }

    function fbLogin() {
        // console.log ($facebook.getVersion());
        console.log("fb login clicked");
        $facebook.login().then(function() {
            console.log($facebook.getAuthResponse());
            AuthenticationService.fbLogin($facebook.getAuthResponse(),function (data) {
                vm.indicator = false;

                if (data.success === true) {
                    console.log(data);
                    $window.location.reload();
                }
                else {
                    vm.error = true;
                }
            });

        });
    }


}]);