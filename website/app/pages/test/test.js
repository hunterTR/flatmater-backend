var app = angular.module('ngCropDrop', ['ui.bootstrap']);
app.factory('ImgService'	, ImgService);//decided to go with factory instead of .service, was having probs
//with .service here, dont know why.
app.directive('ngDrop'		, ngDrop);
app.directive('ngCrop'		, ['$timeout', 'ImgService', ngCrop]);
app.controller('ngDropCtrl'	, ['$scope', '$uibModal','ImgService',ngDropCtrl]);
app.controller('ngCropCtrl'	, ['$scope', '$modalInstance','ImgService', ngCropCtrl]);
;
function ImgService(){
    //service used to store the file data that will be used in dropzone and cropper
    //and ultimately I believe can be used to send the file data upwards
    //to the module that will use this 'plugin'?! idk. still a newbie.
    var dataFactory = {};
    dataFactory.setFile		= function (file) { dataFactory.file = file; };
    dataFactory.getFile		= function() { return dataFactory.file; };
    return dataFactory;
}
function ngCropCtrl($scope, $modalInstance, ImgService) {
    $scope.cropOptions	= {
        maximize		: true,
        strict			: true,
        aspectRatio	: 1,
    };
    $scope.doneCrop = function(){
        ImgService.setFile($scope.avatar.cropper('getCroppedCanvas', { 'width': 300, 'height': 300}));
        $modalInstance.close();
    }
};
function ngDropCtrl($scope, $uibModal, ImgService){
    var dz              = {};//reference on the dropzone; used all over the place.
    $scope.thisFile		= {};//is this useful? idk. should not be I believe.
    $scope.addedFile	= false;
    $scope.dropzoneConfig = {
        'options'   : {
            init: function(){ dz = this; },
            selector    : "#template"
        },
        'eventHandlers': {
            'success': function (file, response) {
                //fileData = response;//is this useful? idk.
            },
            'maxfilesexceeded': function(file){
                //in my example I only want one file in the dropzone.js, that means once
                //the user send a "second" file IF possible, the system 'resets'
                //the dropzone and then and only then it send to the dropzone
                //the new file.
                this.removeAllFiles();
                this.addFile(file);
            },
            'addedfile' : function(file){
                //once the file has been added:
                //1. tell scope I got a new image
                //2. save the data up t the image service
                //3. re render the scope to remove/hide the 'add image/file' button
                //4. add a listener to the preview so it will open the cropper modal.
                $scope.fileData		= file;//is this useful? idk. I think not.
                ImgService.setFile(file);
                $scope.addedFile	= true;
                $scope.$digest();
                file.previewElement.addEventListener("click", $scope.open);
            },
            'complete': function(){
                //will this be useful? Idk. maybe. mostly important on
                //other part of the code so I can upload the full form.
            }
        }//end eventHandlers array
    };//end $scope dropzoneConfig
    $scope.open = function(file) {
        //open the modal and send the dropzone file to the modal.
        var modalInstance = $uibModal.open({
            templateUrl : 'app/pages/test/cropper.html',
            controller  : 'ngCropCtrl',
            resolve: {
                //is this useless?! idk.
                thisFile: function () {
                    return $scope.thisFile;
                }
            }
        });
        modalInstance.result.then(function(){
            //once the modal is closed all this bellow helps sending the file, now cropped, back to dropzone.js
            ImgService.getFile().toBlob(function(blob){
                dz.removeAllFiles();
                //var mockFile = { name: blob.filename, size: blob.size };//useless? idk. maybe.
                //var imageUrl = URL.createObjectURL( blob );//useless? idk. maybe.
                var parts = [blob, new ArrayBuffer()];
                var file = new File(parts, "imageUploadTestFile", {
                    lastModified: new Date(0), // optional - default = now
                    type: "image/jpeg"
                });
                dz.addFile(file);
            });
        });
    };
};//end function ngDropCtrl (dropzone controller)
//dropzone
function ngDrop() {
    return {
        restrict: 'E',
        controller: ngDropCtrl,
        templateUrl : 'app/pages/test/dropzone.html',
        link: function(scope, element, attrs) {
            var config, dropzone;
            config = {
                'options': {
                    init: function () {
                        dz = this;
                    },
                    selector: "#template"
                }
            };
            var configOptions = {
                url                     : "/upload",
                maxFilesize             : 100,
                paramName               : "uploadfile",
                maxThumbnailFilesize    : 5,
                autoProcessQueue        : true,
                maxFiles                : 1,
                thumbnailWidth          : 120,
                thumbnailHeight         : 90,
                parallelUploads         : 20,
                clickable               : ".fileinput-button"// Define the element that should be used as click trigger to select files.
            };
            var previewNode     			= document.querySelector(config.options.selector);
            previewNode.id      			= "";
            var previewTemplate 			= previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            configOptions.init 				= config.options.init;
            configOptions.previewTemplate	= previewTemplate;
            dropzone = new Dropzone(element[0], configOptions);
            angular.forEach(config.eventHandlers, function (handler, event) {
                dropzone.on(event, handler);
            });
            scope.processDropzone	= function() { dropzone.processQueue(); };
            scope.resetDropzone		= function() { dropzone.removeAllFiles(); };
        }
    }
};//end ngDrop ( dropper.js ) directive
function ngCrop($timeout, ImgService) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            scope.imgRef	= URL.createObjectURL(ImgService.getFile());
            var container 	=  element,
                avatarView 		= container.find('.avatar-wrapper'),
                avatar			= avatarView.find('img'),
                imgData			= {},
                avatarPreview	= container.find('.avatar-preview');
            options			= scope.cropOptions;
            options.preview = avatarPreview;
            options.data	= imgData;
            $timeout(
                //need the timeout to begin the cropper otherwise it won't 'get' the avatar and other
                //elements, need a sec so the DOM will load them.
                function(){
                    avatar.cropper(scope.cropOptions);
                    scope.avatar = avatar;
                    scope.imgData = imgData;
                }//end function(){}
            );//end timeout;
        }
    }
};//end ngCrop ( dropzone.js ) directive