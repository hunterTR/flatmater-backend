/**
 * Created by cem.kaya on 26-Oct-15.
 */
'use strict';
angular.module("app.core", [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngMap',
    'ui.router'
]);