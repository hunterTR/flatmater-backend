/**
 * Created by cem.kaya on 29-Nov-15.
 */
'use strict';

angular.module('app.components').controller('SendMessageModalCtrl',['$uibModalInstance','user','MessagesService', function ($uibModalInstance,user,MessagesService) {

    var vm = this;

    vm.user = user;
    vm.sendMessage = sendMessage;
    vm.closeModal = closeModal;

    function closeModal(){
        $uibModalInstance.dismiss('cancel');
    }

    function sendMessage()
    {
        var data = {otheruserid: user.userid, content: vm.content}
        MessagesService.sendMessage(data).then(sendMessageResponse).catch(Error);
    }


    function sendMessageResponse(res) {
        console.log(res);
        if (res.success === true) {
            vm.updateOk = true;
            vm.responseMessage = res.message;
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }

    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }
}]);