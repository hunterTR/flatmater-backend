/**
 * Created by cem.kaya on 26-Nov-15.
 */
angular.module("app.components", [
    'app.profile',
    'ngAnimate',
    'ui.bootstrap'
]);