/**
 * Created by cem.kaya on 26-Nov-15.
 */
'use strict';

angular.module('app.components').controller('CarouselDemoCtrl',['$scope', '$uibModalInstance','user','index', function ($scope, $uibModalInstance,user,index) {
    $scope.myInterval = 5000;
    //$scope.user = user;
    $scope.noWrapSlides = false;
    $scope.slides = [];

    for(var i = 0 ; i < user.photos.length ; i++)
    {
        if(index ===  i)
        {
            $scope.slides.push({
                img:user.photos[i],
                active:true
            });
        }
        else
        {
            $scope.slides.push({
                img:user.photos[i],
                active:false
            });
        }

    }


    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);