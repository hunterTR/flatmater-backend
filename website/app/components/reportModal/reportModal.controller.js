/**
 * Created by cem.kaya on 10-Dec-15.
 */
'use strict';

angular.module('app.components').controller('ReportModalCtrl',['$uibModalInstance','reporteduser','ReportService', function ($uibModalInstance,reporteduser,ReportService) {

    var vm = this;

    vm.user = reporteduser;
    vm.reportUser = reportUser;
    vm.closeModal = closeModal;

    function closeModal(){
        $uibModalInstance.dismiss('cancel');
    }

    function reportUser()
    {
        var data = {reportedid: vm.user.userid, reportreason: vm.reportreason};
        ReportService.reportUser(data).then(reportResponse).catch(Error);
    }


    function reportResponse(res) {
        console.log(res);
        if (res.success === true) {
            vm.updateOk = true;
            vm.responseMessage = res.message;
        }
        else {
            vm.errorMessage = res.message;
            vm.error = true;
        }

    }

    function Error(err) {
        vm.errorMessage = "Problem Occured!";
        vm.error = true;
        console.log(err);
    }
}]);