/**
 * Created by cem.kaya on 28-Nov-15.
 */
'use strict';

angular.module('app.components')
    .service('ReportService',['$http','CookiesService', function ($http,CookiesService) {

        var service ={
            reportUser:reportUser,
        };


        return service;

        function reportUser(data){
            var processUrl = "/api/reportuser";

            return $http.post(processUrl,data,{headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {
            }
        }

    }]);
