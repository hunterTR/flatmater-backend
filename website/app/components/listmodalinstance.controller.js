/**
 * Created by cem.kaya on 26-Nov-15.
 */

angular.module('app.components').controller('ModalInstanceCtrl',['$scope','$state', '$uibModalInstance','$uibModal','user', function ($scope,$state, $uibModalInstance,$uibModal,user) {
    $scope.user = user;

    $scope.navigateToProfile = function(userid){
        $uibModalInstance.dismiss('cancel');
        $state.go("profile",{name:userid});
    }

    $scope.openMessageModal = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/components/sendMessageModal/sendMessageModal.html',
            controller: 'SendMessageModalCtrl',
            controllerAs:'vm',
            resolve:{
                user: function () {
                    return user;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            console.log($scope.selected);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    }
}]);
