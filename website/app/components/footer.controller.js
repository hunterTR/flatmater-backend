/**
 * Created by cem.kaya on 27-Nov-15.
 */
'use strict';

angular.module('app.components')
    .controller('FooterController',['$scope','LanguageService','$uibModal', function ($scope,LanguageService,$uibModal) {

        var vm = this;

        vm.changeLanguage = changeLanguage;
        vm.privacyPolicyModal = privacyPolicyModal;
        vm.termsOfUseModal = termsOfUseModal;


        vm.language = LanguageService.getCurrentLanguage();




        console.log("current language is" + vm.language);
        function changeLanguage(){
            console.log("changing language",vm.language);
            LanguageService.changeLanguage(vm.language);

        }


        function termsOfUseModal(){
            var textfor = {"title" : "Terms of Use", "textToShow" : "asjaoıdjoadajvnfdvsdfvsşdmlamdskclmsdlkvmklmnkljsfdnfdkbl"};
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/components/textShowModal/textShowModal.html',
                controller: 'TextShowModalCtrl',
                controllerAs: 'vm',
                resolve: {
                    text: function () {
                        return textfor;
                    }
                }
            });
        }

        function privacyPolicyModal(){
            var textfor = {"title" : "Privacy Policy", "textToShow" : "asjaoıdjoadajvnfdvsdfvsşdmlamdskclmsdlkvmklmnkljsfdnfdkbl"};
            console.log("privacyPolicy clicked");
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/components/textShowModal/textShowModal.html',
                controller: 'TextShowModalCtrl',
                controllerAs: 'vm',
                resolve: {
                    text: function () {
                        return textfor;
                    }
                }
            });
        }
}]);