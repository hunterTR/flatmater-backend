/**
 * Created by cem.kaya on 29-Jan-16.
 */
'use strict';

angular.module('app.components').controller('TextShowModalCtrl',['$uibModalInstance','text', function ($uibModalInstance,text) {

    var vm = this;
    vm.closeModal = closeModal;
    vm.textToShow = text.textToShow;
    vm.title = text.title;
    function closeModal(){
        $uibModalInstance.dismiss('cancel');
    }

}]);