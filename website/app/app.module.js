'use strict';


var app = angular
    .module('app', [
        'ngFacebook',
        'app.core',
        'app.loading',
        'app.utils',
        'app.login',
        'app.register',
        'app.main',
        'app.listing',
        'app.profile',
        'app.edit',
        'app.navbar',
        'app.category',
        'app.settings',
        'app.components',
        'app.messages',
        'app.verification',
        'app.password',
        'app.test',
        'bootstrapLightbox',
        'angular-md5',
        'angularGrid',
        'pascalprecht.translate',
        'angulartics',
        'angulartics.google.analytics',
    ]).constant('religiousViews',["Atheist", "Agnostic", "Muslim", "Budhist", "Christian"])
      .constant('politicalViews', ["Liberal", "Nationalist", "Right", "Left"])
    .constant('organizations', ["Messy", "Normal", "Tidy"])
    .constant('budgets', ["0-1000", "1000-2000", "2000-3000", "3000-5000", "5000-10000"])
    .constant('horoscopes', ["Aries", "Taurus", "Gemini", "Cancer", "Leo", "Libra",
        "Scorpio", "Sagittarius", "Capicorn", "Aquarius", "Pisces"])
    .constant('sports',["Aeorobics", "Billards/Pool", "Cycling", "Golf",
        "Football", "Running", "Tennis", "Weights", "Volleyball", "Skiing",
        "Dancing", "Yoga", "Martial arts", "Bowling", "Basketball", "Auto Racing", "Sailing"])
    .constant('hobbies', ["Acting", "Board Games", "Cosplaying", "Painting", "Electronics",
        "Gambling", "Homebrewing", "Reading", "Video Games", "Puzzles", "Model Building",
        "Collections", "Fishing", "Hunting"])
    .constant('propertyTypes',["Apartment", "House", "Bungalow", "Cabin", "Villa", "Castle", "Dorm"])
    .config(['$stateProvider','$urlRouterProvider','$locationProvider', '$facebookProvider', 'LightboxProvider','$translateProvider',function ($stateProvider, $urlRouterProvider,$locationProvider, $facebookProvider, LightboxProvider,$translateProvider) {

        //LANGUAGE CONFIG

        $translateProvider.useSanitizeValueStrategy('sanitize');
        $translateProvider.useStaticFilesLoader({
            prefix: 'app/extras/languages/language-',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');


//FACEBOOK CONFIG
        $facebookProvider.setAppId('724514177679956');
        $facebookProvider.setCustomInit({version: 'v2.5'});
        $facebookProvider.setPermissions('email,user_likes');

//LIGHTBOX CONFIG
        LightboxProvider.templateUrl = 'app/pages/profile/edit/photoGallery/galleryLightBoxTemplate.html';


        // increase the maximum display height of the image
        LightboxProvider.calculateImageDimensionLimits = function (dimensions) {
            return {
                'maxWidth': dimensions.windowWidth >= 768 ? // default
                dimensions.windowWidth - 92 :
                dimensions.windowWidth - 52,
                'maxHeight': 1600                           // custom
            };
        };

        // the modal height calculation has to be changed since our custom template is
        // taller than the default template
        LightboxProvider.calculateModalDimensions = function (dimensions) {
            var width = Math.max(400, dimensions.imageDisplayWidth + 32);

            if (width >= dimensions.windowWidth - 20 || dimensions.windowWidth < 768) {
                width = 'auto';
            }

            return {
                'width': width,                             // default
                'height': 'auto'                            // custom
            };
        };




//ROUTE CONFIG

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/pages/main/main.html',
                controller: 'MainCtrl',
                controllerAs: "vm"
            }).state('loading', {
            url: '/loading/:stateName/:stateParams',
            templateUrl: 'app/pages/loading/loading.html',
            controller: 'LoadingCtrl',
            controllerAs: "vm"
        }).state('login', {
            url: '/login',
            templateUrl: 'app/pages/login/login.html',
            controller: 'LoginCtrl',
            controllerAs: "vm",
            login: false
        }).state('register', {
            url: '/register',
            templateUrl: 'app/pages/register/register.html',
            controller: 'RegisterCtrl',
            controllerAs: "vm",
            login: false
        }).state('listing', {
            url: '/listing',
            templateUrl: 'app/pages/listing/listing.html',
            controller: 'ListingCtrl',
            controllerAs: "vm",
            login: true
        }).state('edit', {
            url: '/edit',
            templateUrl: 'app/pages/profile/edit/edit.html',
            controller: 'EditProfileCtrl',
            controllerAs: "vm",
            login: true
        }).state('category', {
            url: '/category/:name',
            templateUrl: 'app/pages/category/category.html',
            controller: 'CategoryCtrl',
            controllerAs: "vm"
        }).state('settings', {
            url: '/settings',
            templateUrl: 'app/pages/settings/settings.html',
            controller: 'SettingsCtrl',
            controllerAs: 'vm',
            login:true
        }).state('profile', {
            url: '/profile/:name',
            templateUrl: 'app/pages/profile/profile.html',
            controller: 'ProfileCtrl',
            controllerAs: "vm"
        }).state('donate', {
            url: '/donate',
            templateUrl: 'app/pages/donate/donate.html'
        }).state('messages', {
            url: '/messages',
            templateUrl: 'app/pages/messages/messages.html',
            controller: 'MessagesCtrl',
            controllerAs: 'vm',
            login:true
        }).state('message', {
            url: '/message/:name',
            templateUrl: 'app/pages/messages/messageModal/messageModal.html',
            controller: 'MessageModalCtrl',
            controllerAs: 'vm',
            login:true
        }).state('verification', {
            url: '/verification/:name',
            templateUrl: 'app/pages/verification/verification.html',
            controller: 'VerificationCtrl',
            controllerAs: 'vm',
            login:true,
            verified:false
        }).state('password', {
            url: '/password/:name',
            templateUrl: 'app/pages/password/password.html',
            controller: 'PasswordCtrl',
            controllerAs: 'vm'
        }).state('test', {
            url: '/test',
            templateUrl: 'app/pages/test/test.html',
            controller: 'TestCtrl',
            controllerAs: 'vm'
        });

        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);

    }]).run(['$state', 'AuthenticationService', 'ApplicationService', '$window', '$location', '$rootScope',function ($state, AuthenticationService, ApplicationService, $window, $location, $rootScope) {

        (function () {
            // If we've already installed the SDK, we're done
            console.log("in facebook function");
            if (document.getElementById('facebook-jssdk')) {
                return;
            }

            // Get the first script element, which we'll use to find the parent node
            var firstScriptElement = document.getElementsByTagName('script')[0];

            // Create a new script element and set its id
            var facebookJS = document.createElement('script');
            facebookJS.id = 'facebook-jssdk';

            // Set the new script's source to the source of the Facebook JS SDK
            facebookJS.src = '//connect.facebook.net/en_US/sdk.js"';

            // Insert the Facebook JS SDK into the DOM
            firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
        }());


        AuthenticationService.requestCurrentUser()
            .then(function () {
                ApplicationService.makeReady();
                console.log("making ready");
            });


        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            //all cases will be redirect to loading firstly

            console.log(toState);
            console.log(toParams);
            console.log(fromState);

            if (toState.name === 'loading') return;

            if (!ApplicationService.isReady()) {
                if (fromState.name === 'loading') return;

                event.preventDefault();
                $state.go('loading', {stateName: toState.name,stateParams:toParams.name});
                //   $location.path('/loading/' + toState.name);
            }

            if (ApplicationService.isReady()) {
                console.log("application is ready");
                //Checking login required or not.
                if (toState.login != undefined && toState.login != null && toState.login === true) {
                    console.log("should not be shown to guests!");
                    if (AuthenticationService.getCurrentUser() == undefined || AuthenticationService.getCurrentUser() == null || AuthenticationService.getCurrentUser().success === false) {
                        event.preventDefault();
                        $state.go('login');
                    }

                }
                else if (toState.login != undefined && toState.login != null && toState.login === false) {
                    console.log("should not be shown to logged in users!")
                    if (AuthenticationService.getCurrentUser().success === true) {
                        event.preventDefault();
                        $state.go('home');
                    }
                }
            }


        });

        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams){
                console.log("state change success");
                if($location.hash()) $anchorScroll(); })
    }]);