/**
 * Created by cem.kaya on 27-Oct-15.
 */
'use strict';
angular.module('app.utils')
    .service('InformationService',['AuthenticationService', function (AuthenticationService) {

        var informations = null;
        var priorities = null;
        var categorySelection = {};

        var service = {
            setInformations : setInformations,
            getInformations : getInformations,
            setPriorities : setPriorities,
            getPriorities : getPriorities,
            setCategorySelection: setCategorySelection,
            getCategorySelection: getCategorySelection
        };

        return service;

        function setInformations(data){
            informations = data;
        }

        function getInformations()
        {
            return informations;
        }

        function setPriorities(data)
        {
            priorities = data;
        }

        function getPriorities()
        {
            return priorities;
        }
        function setCategorySelection(data)
        {
            categorySelection.selection = data;
        }

        function getCategorySelection(data)
        {
            return categorySelection;
        }

    }]);
