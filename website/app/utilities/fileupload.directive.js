/**
 * Created by cem.kaya on 01-Nov-15.
 */
'use strict';

angular.module("app.utils")
    .directive("dropzone", [dropzone]);

function dropzone() {

    return {
        restrict: 'C',
        link: function (scope, element, attrs) {

            var config = {
                url: 'http://localhost:3000/upload',
                maxFilesize: 1,
                maxThumbnailFilesize: 2,
                maxFiles: 2,
                paramName: "ProfilePhotoFile",
                acceptedFiles: '.jpg,.jpeg,.png,.gif',
                dictInvalidFileType: "Desteklenmeyen dosya tipi!",
                parallelUploads: 1,
            };

            config = JSON.parse(attrs.config);
            console.log(config);

            var settings = scope["settings"];

            console.log(scope);

            //var eventHandlers = {
            //    'addedfile': function (file) {
            //
            //    },
            //
            //    'success': function (file, response) {
            //        console.log(response);
            //        if (response.success === true) {
            //            console.log("yeeeee");
            //
            //        } else {
            //        }
            //    }
            //
            //};

            dropzone = new Dropzone(element[0], config);

            angular.forEach(settings.eventHandlers, function (handler, event) {
                dropzone.on(event, handler);
            });

            scope.processDropzone = function () {
                dropzone.processQueue();
            };

            //scope.resetDropzone = function () {
            //    dropzone.removeAllFiles();
            //}
        }
    }

}
