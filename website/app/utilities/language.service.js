/**
 * Created by cem.kaya on 27-Nov-15.
 */

'use strict';

angular.module('app.utils')
    .service('LanguageService',['$rootScope','$q','$http', '$translate', 'CookiesService', function ($rootScope,$q,$http, $translate, CookiesService) {




        var currentLanguage = $translate.use();
        getLocationFromIpAddress().then(function (res) {
            currentLanguage = res;
        });

        var service = {
            changeLanguage: changeLanguage,
            getCurrentLanguage: getCurrentLanguage,
            getLocationFromIpAddress : getLocationFromIpAddress
        };
        return service;


        function getLocationFromIpAddress() {
            var deferred = $q.defer();
            var lang = CookiesService.getFromCookies('lang');

            if (lang !== undefined && lang !== null) {
                console.log('Language is already selected. ' + lang);

                currentLanguage = lang;
                $rootScope.language = lang;
                $translate.use(lang);
                deferred.resolve(lang);
            }
            else {
                $http({     // get country from IP address
                    method: 'GET',
                    url: 'http://ipinfo.io/json',
                }).
                success(function (data, status, headers, config) {
                    console.log(data);
                    if (data.country === "TR") {
                        currentLanguage = "tr";
                        $translate.use("tr");
                        $rootScope.language = "tr";
                        deferred.resolve(currentLanguage);
                    }
                    else if (data.country === "DE") {
                        currentLanguage = "de";
                        $translate.use("de");
                        $rootScope.language = "de";
                        deferred.resolve(currentLanguage);
                    }
                    else {
                        currentLanguage = "en";
                        $rootScope.language = "en";
                        $translate.use("en");
                    }

                }).
                error(function (data, status, headers, config) {
                    $log.warn(data, status, headers(), config);
                    deferred.resolve(currentLanguage);
                });

            }

            return deferred.promise;
        }


        function changeLanguage(lang) {
            $translate.use(lang);
            $rootScope.language = $translate.use();
            CookiesService.saveToCookies('lang', lang);
        }

        function getCurrentLanguage() {
            return currentLanguage;
        }


    }]);