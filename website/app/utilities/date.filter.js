/**
 * Created by cem.kaya on 10-Nov-15.
 */
angular.module('app.utils').filter('dateFormat', ['$filter',function($filter)
{
    return function(input)
    {
        if(input == null){ return ""; }

        var _date = $filter('date')(new Date(input), 'dd/MM/yyyy');

        return _date.toUpperCase();

    };
}]).filter('dateWithTimeFormat', ['$filter',function($filter)
{
    return function(input)
    {
        if(input == null){ return ""; }

        var _date = $filter('date')(new Date(input), 'dd/MM/yyyy HH:mm:ss');

        return _date.toUpperCase();

    };
}]);