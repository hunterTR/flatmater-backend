/**
 * Created by cem.kaya on 27-Oct-15.
 */
'use strict';

angular.module('app.utils')
    .service('ApplicationService',['AuthenticationService','$http','$translate', function (AuthenticationService,$http,$translate) {

        var ready = false;
        var registeredListeners = [];


        var callListeners = function () {
            for (var i = registeredListeners.length - 1; i >= 0; i--) {
                registeredListeners[i]();
            }
        };


        var service = {
            makeReady: makeReady,
            isReady: isReady,
            registerListener: registerListener,
        };
        return service;


        function isReady() {
            return ready;
        }

        function makeReady() {
            ready = true;
            callListeners();
        }

        function registerListener(callback) {
            if (ready) callback();
            else registeredListeners.push(callback);
        }




    }]);