/**
 * Created by cem.kaya on 27-Oct-15.
 */
'use strict';

angular.module('app.utils')
    .directive("matchpw", function () {
        return {
            require: "ngModel",
            scope: {
                matchpw: '='
            },
            link: function (scope, element, attrs, ctrl) {
                scope.$watch(function () {
                    var combined;

                    if (scope.passwordVerify || ctrl.$viewValue) {
                        combined = scope.matchpw + '_' + ctrl.$viewValue;
                    }
                    return combined;
                }, function (value) {
                    if (value) {
                        ctrl.$parsers.unshift(function (viewValue) {
                            var origin = scope.matchpw;
                            if (origin !== viewValue) {
                                ctrl.$setValidity("matchpw", false);
                                return undefined;
                            } else {
                                ctrl.$setValidity("matchpw", true);
                                return viewValue;
                            }
                        });
                    }
                });
            }
        };
    });