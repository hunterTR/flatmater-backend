'use strict';

/**
 * @ngdoc service
 * @name yoangularApp.AuthenticationService
 * @description
 * # AuthenticationService
 * Service in the yoangularApp.
 */
angular.module('app.utils')
    .service('AuthenticationService',['$timeout', '$q', '$http', 'CookiesService', '$log','$window', function ($timeout, $q, $http, CookiesService, $log,$window) {
        var isLoggedIn = false;
        if (CookiesService.getFromCookies('token') != null && CookiesService.getFromCookies('token') != undefined) {
            isLoggedIn = true;
        } else {
            isLoggedIn = false;

        }
        var authenticatedUser = {'success': false};
        var services = {
            getCurrentUser: getCurrentUser,
            register: register,
            logout: logout,
            login: login,
            fbLogin: fbLogin,
            sendValidationMail: sendValidationMail,
            recoverPassword: recoverPassword,
            changeForgottenPassword: changeForgottenPassword,
            isLoggedIn: isLoggedIn,
            requestCurrentUser: requestCurrentUser,
            getTest: getTest
        }

        return services;

        function getTest() {
            return authenticatedUser;
        }

        function requestCurrentUser() {
            var processUrl = "/api/getuserbytoken";

            var deferred = $q.defer();

            $http.post(processUrl, {}, {headers: {'x-access-token': CookiesService.getFromCookies('token')}})
                .success(function (res) {
                    $timeout(function () {
                        console.log(res);
                        if (res) {
                            authenticatedUser = res;
                            if (res.success === false) {
                                tryAuthWithRefreshToken(callbackForAuthWithRefresh);
                            }
                            else {
                                deferred.resolve(authenticatedUser);
                            }
                        }
                        function callbackForAuthWithRefresh(res) {
                            if(res.success===true)
                            {
                               $window.location.reload();

                            }else
                            {

                                removeToken();
                                deferred.resolve(res);
                            }

                        }

                    }, 500);
                })
                .error(function (err) {
                    console.log(err);

                    authenticatedUser.success = false;
                    deferred.resolve(authenticatedUser);
                });

            return deferred.promise;
        }

        function tryAuthWithRefreshToken(callback) {
            $http({
                method: 'POST',
                url: '/authwithrefreshtoken',
                headers: {
                    'x-access-token': CookiesService.getFromCookies('token'),
                    'x-refresh-token': CookiesService.getFromCookies('refreshtoken')
                }
            }).
            success(function (data, status, headers, config) {
                if (data.success === true) {
                    CookiesService.saveToCookies('token', data.token);
                    CookiesService.saveToCookies('refreshtoken', data.refreshtoken);
                    //$window.location.reload();
                }
                callback(data);
                console.log(data);
            }).
            error(function (data, status, headers, config) {
                $log.warn(data, status, headers(), config);
            });
        }

        function removeToken() {
            console.log("token is removed");
            CookiesService.saveToCookies('token', null);
            CookiesService.saveToCookies('refreshtoken', null);
        }


        function getCurrentUser() {
            return authenticatedUser;
            console.log("getting current user");
            console.log(authenticatedUser);
        }

        function fbLogin(fbAuthResponse, callback) {
            $http({
                method: 'POST',
                url: '/fblogin',
                data: fbAuthResponse
            }).
            success(function (data, status, headers, config) {
                if (data.success === true) {
                    CookiesService.saveToCookies('token', data.token);
                    CookiesService.saveToCookies('refreshtoken', data.refreshtoken);
                }
                callback(data);
                console.log(data);
            }).
            error(function (data, status, headers, config) {
                $log.warn(data, status, headers(), config);
            });

        }

        function register(data) {
            var processUrl = "/register";

            return $http.post(processUrl, data)
                .then(response)
                .catch(error);

            function response(res) {
                return res.data;
            }

            function error(err) {

            }
        }

        function logout() {
            CookiesService.saveToCookies('token', null);
            isLoggedIn = false;
        }

        function login(email, password, callback) {
            $http({
                method: 'POST',
                url: '/api/authenticate',
                data: {
                    'email': email,
                    'password': password
                }
            }).
            success(function (data, status, headers, config) {
                if (data.success === true) {
                    CookiesService.saveToCookies('token', data.token);
                    CookiesService.saveToCookies('refreshtoken', data.refreshtoken);
                }
                callback(data);
                console.log(data);
            }).
            error(function (data, status, headers, config) {
                $log.warn(data, status, headers(), config);
            });
        }

        function sendValidationMail() {

        }

        function recoverPassword(datas, callback) {
            $http({
                method: 'POST',
                url: '/passwordforgot',
                data: datas
            }).
            success(function (data, status, headers, config) {
                callback(data);
                console.log(data);
            }).
            error(function (data, status, headers, config) {
                $log.warn(data, status, headers(), config);
            });
        }

        function changeForgottenPassword() {

        }


    }]);
