/**
 * Created by cem.kaya on 26-Oct-15.
 */
'use strict';

/**
 * @ngdoc service
 * @name yoangularApp.CookieStoreService
 * @description
 * # CookieStoreService
 * Service in the yoangularApp.
 */
angular.module('app.utils')
    .service('CookiesService',['$cookies','$window', function ($cookies,$window) {

        this.saveToCookies = function(cookieName,cookie,dayFurther)
        {

            if(dayFurther === null || dayFurther === undefined)
            {
                dayFurther = 6;
            }
            var now = new $window.Date(),
            // this will set the expiration to 6 months
                exp = new $window.Date(now.getFullYear(), now.getMonth(), now.getDate()+dayFurther);

            $cookies.put(cookieName,cookie,{
                expires: exp
            });

        }

        this.getFromCookies = function(cookieName)
        {
            return $cookies.get(cookieName);
        }

    }]);
