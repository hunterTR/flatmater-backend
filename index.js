var express = require("express");
var mongo = require("mongodb");
var host = "127.0.0.1";
var port = "27017";
var app = express();
var apiRoutes = express.Router();
var connect = require('connect');
var bodyParser = require('body-parser');
var collUsers;
var User = require('user');
var http = require('http');
var https = require('https')
var fs = require('fs');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var path = require('path');
var Busboy = require('busboy');
var utilityModule = require("utility");
var utility = new utilityModule();
var cors = require('cors');

var limiter = require('connect-ratelimit');
var RateLimit = require('express-rate-limit');

var useractions = require('UserRequests');
var userfunctions = require('UserFunctions');
// create reusable transporter object using SMTP transport

// NB! No need to recreate the transporter object. You can use
// the same transporter object for all e-mails

// setup e-mail data with unicode symbols


var roothPath = path.normalize(__dirname + '/website');

app.use(express.static(roothPath));


// default options, no immediate parsing

// default options shown below 
var limiter = RateLimit({
    // window, delay, and max apply per-ip unless global is set to true 
    windowMs: 2 * 1000, // miliseconds - how long to keep records of requests in memory 
    delayMs: 0, // milliseconds - base delay applied to the response - multiplied by number of recent hits from user's IP 
    max: 8, // max number of recent connections during `window` miliseconds before (temporarily) bocking the user. 
    global: false // if true, IP address is ignored and setting is applied equally to all requests 
});

//app.use(limiter);


//AMAZON S3
//var knox = require('knox').createClient({
//    key: 'AKIAJEM6EU4SVLQJUOUA'
//  , secret: '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'
//  , region: 'eu-west-1'
//  , bucket: 'dedikoduapp'
//});



var db = new mongo.Db("flatmater", new mongo.Server(host, port, {}));
db.open(function (error) {
    console.log("Connected to" + host + ":" + port);

    db.collection("messages", function (error, collection) {

        console.log("messages collection created");

        var collMessages = collection;

        db.collection("reports", function (error, collection) {

            console.log("reports collection created");

           var collReports = collection;

            var callback = function (result) {
                console.log(result);
            };

            db.collection("users", function (error, collection) {

                console.log("users collection created");

                collUsers = collection;

                var callback = function (result) {
                    console.log(result);
                };

                var userfuncs = new userfunctions(collUsers, collMessages,collReports);
                var useract = new useractions(app, userfuncs);

            });

        });





        ////////////TESTING/////////////////
        //   var json = { "username": "uzuna3", "password": "crypto",  "email": "cccvcd@gmail.com" };
        //    var tmp = new User(json);
        //   userfuncs.insertNewUser(tmp, callback);
    });


});


//app.use(cors({credentials: true}));
// parse application/x-www-form-urlencoded
app.use(bodyParser.json());


app.post('/upload', function (req, res) {
    var success = false;
    var message = "";
    var link = null;
    var busboyy = new Busboy({headers: req.headers, limit: {fileSize: 2097152}});
    busboyy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log("filename " + filename);
        var extname = path.extname(filename);
        console.log("Extension name : " + extname);
        filename = utility.randomString(30) + extname;
        var saveTo = path.join(__dirname + "/uploads", filename);
        console.log('Uploading: ' + saveTo);

        file.on('data', function (data) {
            console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
        });
        file.on('end', function () {
            console.log('File [' + fieldname + '] Finished');

            console.log("file has been uploaded " + filename);

        });

        file.pipe(fs.createWriteStream(saveTo));


    });

    busboyy.on('finish', function () {
        console.log('Upload complete');
        // res.writeHead(200, { 'Connection': 'close' });
        res.json({success: success, message: message, link: link});
    });
    return req.pipe(busboyy);
});

apiRoutes.post('/authenticate', function (req, res) {

    console.log(req.body);
    // find the user
    collUsers.findOne({
        email: req.body.email,
        status:{$ne:"deleted"}
    }, function (err, user) {

        if (err) throw err;

        if (!user) {
            res.json({success: false, message: 'Authentication failed. User not found.'});
        } else if (user) {


            var hash = crypto.createHash('sha1').update(req.body.password).digest('hex');
            //console.log(hash);

            hash = crypto.createHash('sha1').update(hash).digest('hex');
            //console.log(hash);
            // check if password matches
            if (user.secret.password != hash) {
                // res.redirect('google.com');
                res.json({success: false, message: 'Authentication failed. Wrong password.'});
            } else {

                // if user is found and password is right
                // create a token
                var payload = {
                    userid: user.userid,
                    username: user.email
                };
                var token = jwt.sign(payload, "flatmatersupersecretcode", {
                    expiresIn: 100000,
                    algorithm: 'HS512'
                });
                var refreshtoken = utility.randomString(18) + user.userid;

                var refreshhash= crypto.createHash('sha1').update(refreshtoken).digest('hex');

                hash = crypto.createHash('sha1').update(token).digest('hex');

                collUsers.update({"email": user.email}, {
                    $set: {
                        "secret.token": hash,
                        "secret.clientpasswordtoken": null,
                        "secret.serverpasswordtoken": null,
                        "secret.refreshtoken": refreshhash,
                        "lastlogin": new Date(),
                        "signInCount": user.signInCount + 1
                    }
                }, function (err, result) {

                    console.log(user.email + " has logged in. Token: " + token);

                    res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token,
                        refreshtoken:refreshtoken
                    });


                });


            }

        }

    });
});


apiRoutes.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    //var token = req.body.token || req.params.token || req.headers['x-access-token'];
    var token = req.headers['x-access-token'];

    console.log(token);
    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, "flatmatersupersecretcode", function (err, decoded) {
            if (err) {
                console.log('Failed to authenticate token.');
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }

});


//Image things

app.get('/uploads/*', function (req, response) {
    var filePath = path.join(__dirname, req.url);
    console.log(filePath);

    fs.exists(filePath, function (exists) {
        if (exists) {
            var stat = fs.statSync(filePath);
            console.log("sending the image");
            response.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': stat.size
            });

            fs.createReadStream(filePath).pipe(response);
        }
        else {
            console.log("no image found. sending default image");
            filePath = path.join(__dirname,
                "/uploads/test.jpg");
            var stat = fs.statSync(filePath);


            response.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': stat.size
            });

            fs.createReadStream(filePath).pipe(response);
        }
    });

});

app.get('*', function (req, res) {
  //  console.log(__dirname);
    res.sendFile(__dirname + '/website/index.html');
});



// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------
apiRoutes.get('/', function (req, res) {
    res.json({message: 'Welcome to the flatmater API!'});
});


app.use('/api', apiRoutes);

//app.use(function(req,resp,next){
//    if (req.headers['x-forwarded-proto'] == 'http') {
//        return resp.redirect(301, 'https://' + req.headers.host + '/');
//    } else {
//        return next();
//    }
//});

http.createServer(function(req, res) {
    res.writeHead(301, {
        Location: "https://www." + req.headers["host"].replace("www.", "") + req.url
    });
    res.end();
}).listen(80);

var server = https.createServer({
    key: fs.readFileSync('theflatmater_com.key'),
    cert: fs.readFileSync('theflatmater_com.crt')
    }, app).listen(443, function(){
    console.log("Listening on port");
});
